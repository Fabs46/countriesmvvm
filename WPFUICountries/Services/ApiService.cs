﻿
namespace WPFUICountries.Services
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;
    using WPFUICountries.Models;

    public class ApiService
    {

        public async Task<Response> GetCountries(string urlBase, string controller)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);

                var response = await client.GetAsync(controller);

                var result = await response.Content.ReadAsStringAsync();


                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }

                var all = JsonConvert.DeserializeObject<List<Country>>(result);

                return new Response
                {
                    IsSuccess = true,
                    Result = all,
                };
        }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message,
                };
}
        }

        public async Task<Response> GetMeteos(string urlBase, string controller)
        {
            //try
            //{
            var client = new HttpClient();
            client.BaseAddress = new Uri(urlBase);

            var response = await client.GetAsync(controller);

            var result = await response.Content.ReadAsStringAsync();


            if (!response.IsSuccessStatusCode)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = result,
                };
            }

            var all = JsonConvert.DeserializeObject<List<Meteo>>(result);

            return new Response
            {
                IsSuccess = true,
                Result = all,
            };
            //}
            //catch (Exception ex)
            //{
            //    return new Response
            //    {
            //        IsSuccess = false,
            //        Message = ex.Message,
            //    };
            //}
        }
    }
}
