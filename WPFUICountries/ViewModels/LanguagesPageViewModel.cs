﻿
namespace WPFUICountries.ViewModels
{
    using Caliburn.Micro;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using WPFUICountries.Models;
    using WPFUICountries.Services;

    public class LanguagesPageViewModel : Screen
    {
        private Country _selectedCountry;

        private List<Language> _selectedLanguage;

        public Country SelectedCountry
        {
            get { return _selectedCountry; }
            set
            {
                _selectedCountry = value;
                NotifyOfPropertyChange(() => SelectedCountry);

                if (SelectedCountry != null)
                {
                    SelectedLanguage = SelectedCountry.Languages;
                }
            }
        }

        public List<Language> SelectedLanguage
        {
            get
            {
                return _selectedLanguage;
            }
            set
            {
                _selectedLanguage = SelectedCountry.Languages;
                NotifyOfPropertyChange(() => SelectedLanguage);//to show Property when changed
            }
        }


        public LanguagesPageViewModel(Country selectedCountry, List<Language> language)
        {
            _selectedLanguage = language;
            _selectedCountry = selectedCountry;
        }

    }
}
