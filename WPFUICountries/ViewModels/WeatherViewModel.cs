﻿namespace WPFUICountries.ViewModels
{
    using Caliburn.Micro;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using WPFUICountries.Models;
    using WPFUICountries.Services;

    public class WeatherViewModel
    {

        private string _selectedCountryCapital;

        private string _selectedMeteoName;

        private List<Meteo> Meteos;

        private Meteo _selectedMeteo;

        private BindableCollection<Meteo> _meteo = new BindableCollection<Meteo>();

        private NetworkService _networkService = new NetworkService();

        private ApiService _apiService = new ApiService();

        public BindableCollection<Meteo> BCMeteo
        {
            get
            {
                return _meteo;
            }
            set
            {
                _meteo = value;
            }
        }

        public string SelectedCountryCapital
        {
            get { return _selectedCountryCapital; }
            set
            {
                _selectedCountryCapital = value;
                //NotifyOfPropertyChange(() => SelectedCountryCapital);
            }
        }

        public string SelectedMeteoName
        {
            get { return _selectedMeteoName; }
            set
            {
                _selectedMeteoName = value;
               //NotifyOfPropertyChange(() => );
            }
        }

        public WeatherViewModel(Country selectedCountry, Meteo selectedMeteo)
        {
            this.SelectedCountryCapital = selectedCountry.Capital;
            LoadMeteo(selectedCountry);
            //this.SelectedMeteoName = selectedMeteo.name;
        }

        //Method to renderize from List to Bindable
        public void ListToBindableMeteo()
        {
            BCMeteo.AddRange(Meteos);
        }

        private async void LoadMeteo(Country SelectedCountry)
        {
            bool load;

            var connection = _networkService.CheckConnection();

            if (!connection.IsSuccess)
            {
                ///TODO LoadLocalCountries();
                //LoadLocalCountries();
                load = false;
            }
            else
            {
                await LoadApiMeteo(SelectedCountry);
                load = true;
            }

            //if(Countries.Count == 0)
            //{
            //    MessageBoxResult result = MessageBox.Show("No Internet connection" + Environment.NewLine +
            //        "The Countries weren't loaded" + Environment.NewLine +
            //        "try it later!");

            //}
        }

        public async Task LoadApiMeteo(Country SelectedCountry)
        {
            var response = await _apiService.GetMeteos("api.openweathermap.org/data/2.5/weather", $"/data/2.5/weather?q={SelectedCountry.Capital}&APPID=d0e5ce283b47ed259dc7f9c819990ac3");

            Meteos = (List<Meteo>)response.Result;

            ListToBindableMeteo();

            //dataService.DeleteData();

            //dataService.SaveData(Countries);
        }
    }
}
