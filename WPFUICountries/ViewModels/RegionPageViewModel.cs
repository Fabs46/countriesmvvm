﻿
namespace WPFUICountries.ViewModels
{
    using Caliburn.Micro;
    using Svg;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using WPFUICountries.Models;
    using WPFUICountries.Services;

    public class RegionPageViewModel : Screen
    {

        private string _selectedCountryCapital;

        private string _selectedCountryRegion;

        private string _selectedCountrySubregion;

        private int _selectedCountryPopulation;

        private string _selectedCountryGini;

        private string _selectedCountryNativeName;

        private string _selectedCountryFlag;

        private string _selectedCountryAlpha3Code;

        private string _Flag;

        public string Flag {

            get
            {
                return _Flag;
            }

            set
            {
                _Flag = value;
                NotifyOfPropertyChange(() => Flag);
            }

        }

        public string SelectedCountryCapital

        {
            get { return _selectedCountryCapital; }
            set
            {
                _selectedCountryCapital = value;
                NotifyOfPropertyChange(() => SelectedCountryCapital);
            }
        }

        public string SelectedCountryRegion
        {
            get { return _selectedCountryRegion; }
            set
            {
                _selectedCountryRegion = value;
                NotifyOfPropertyChange(() => SelectedCountryRegion);
            }
        }

        public string SelectedCountrySubregion
        {
            get { return _selectedCountrySubregion; }
            set
            {
                _selectedCountrySubregion = value;
                NotifyOfPropertyChange(() => SelectedCountrySubregion);
            }
        }

        public int SelectedCountryPopulation
        {
            get { return _selectedCountryPopulation; }
            set
            {
                _selectedCountryPopulation = value;
                NotifyOfPropertyChange(() => SelectedCountryPopulation);
            }
        }

        public string SelectedCountryGini
        {
            get { return _selectedCountryGini; }
            set
            {
                _selectedCountryGini = value;
                NotifyOfPropertyChange(() => SelectedCountryGini);
            }
        }

        public string SelectedCountryNativeName
        {
            get { return _selectedCountryNativeName; }
            set
            {
                _selectedCountryNativeName = value;
                NotifyOfPropertyChange(() => SelectedCountryNativeName);
            }
        }

        public string SelectedCountryFlag
        {
            get { return _selectedCountryFlag; }
            set
            {
                _selectedCountryFlag = value;
                NotifyOfPropertyChange(() => SelectedCountryFlag);
            }
        }

        public string SelectedCountryAlpha3Code
        {
            get { return _selectedCountryAlpha3Code; }
            set
            {
                _selectedCountryAlpha3Code = value;
                NotifyOfPropertyChange(() => SelectedCountryAlpha3Code);
            }
        }

        public RegionPageViewModel( Country selectedCountry)
        {

            this.SelectedCountryCapital = selectedCountry.Capital;
            this.SelectedCountryRegion = selectedCountry.Region;
            this.SelectedCountrySubregion = selectedCountry.Subregion;
            this.SelectedCountryPopulation = selectedCountry.Population;
            this.SelectedCountryGini = selectedCountry.Gini.ToString();
            this.SelectedCountryNativeName = selectedCountry.NativeName;
            this.SelectedCountryAlpha3Code = selectedCountry.Alpha3Code;
            this.SelectedCountryFlag = selectedCountry.Flag;

            ///TODO Validation for general Info
            if (selectedCountry.Gini == null)
            {
                SelectedCountryGini = "Information not available ";
            }
            if (selectedCountry.Capital == string.Empty)
            {
                SelectedCountryCapital = "Information not available ";
            }
            if (selectedCountry.Region == string.Empty)
            {
                SelectedCountryRegion = "Information not available ";
            }
            if (selectedCountry.Subregion == string.Empty)
            {
                SelectedCountrySubregion = "Information not available ";
            }
            if (selectedCountry.NativeName == string.Empty)
            {
                SelectedCountryNativeName = "Information not available ";
            }
            if (selectedCountry.Alpha3Code == string.Empty)
            {
                SelectedCountryAlpha3Code = "Information not available ";
            }
            
            using (WebClient client = new WebClient())
            {
                //string url = $"https://restcountries.eu/data/{selectedCountry.Alpha3Code.ToLower()}.svg";
                string url = selectedCountry.Flag;

                if (!Directory.Exists(@".\Media"))
                {
                    Directory.CreateDirectory(@".\Media");
                }
                if (!(new FileInfo($@".\Media\{selectedCountry.Alpha3Code.ToLower()}.svg").Exists))
                {
                    client.DownloadFileCompleted += Client_DownloadFileCompleted;
                    client.DownloadFileAsync(new Uri(url), $@".\Media\{selectedCountry.Alpha3Code.ToLower()}.svg");
                }
                else
                {
                    Client_DownloadFileCompleted(null, null);
                }
            }
        }

        private void Client_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            this.Flag = $@".\Media\{ this.SelectedCountryAlpha3Code.ToLower()}.svg";
        }
    }
}
