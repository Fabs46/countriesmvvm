﻿
namespace WPFUICountries.ViewModels
{
    using Caliburn.Micro;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using WPFUICountries.Models;
    using WPFUICountries.Services;
    using WPFUICountries.Views;

    public class ShellViewModel : Conductor<object>
    {

        private BindableCollection<Country> _country = new BindableCollection<Country>();

        private List<Country> Countries;

        private Country _selectedCountry;

        private Meteo _selectedMeteo;

        private NetworkService _networkService = new NetworkService();

        private ApiService _apiService = new ApiService();

        private DialogService DialogService = new DialogService();

        private List<Language> _language;



        public BindableCollection<Country> Country
        {
            get
            {
                return _country;
            }
            set
            {
                _country = value;
            }
        }

        public Country SelectedCountry

        {
            get
            {
                return _selectedCountry;
            }
            set
            {
                _selectedCountry = value;
                NotifyOfPropertyChange(() => SelectedCountry);
                NotifyOfPropertyChange(() => SelectedMeteo.name);

                if (SelectedCountry != null && ActiveItem != null)
                {
                    Language = SelectedCountry.Languages;
                    //SelectedMeteo.name = SelectedCountry.Capital;

                    if (typeof(LanguagesPageViewModel) == ActiveItem.GetType())
                    {
                        LoadPageLanguages();
                    }
                    if (typeof(GeneralPageViewModel) == ActiveItem.GetType())
                    {
                        LoadPageRegion();
                    }
                    if (typeof(WeatherViewModel) == ActiveItem.GetType())
                    {
                        LoadPageWeather();
                    }

                    //Old way to do it
                    //switch (ActiveItem.GetType().Name)
                    //{
                    //    case "RegionPageViewModel":
                    //        {
                    //            LoadPageRegion();
                    //        }
                    //        break;

                    //    case "LanguagesPageViewModel":
                    //        {

                    //            LoadPageLanguages();
                    //        }
                    //        break;

                    //    case "WeatherViewModel":
                    //        {
                    //            LoadPageWeather();
                    //        }
                    //        break;
                    //}
                }
            }
        }

        public Meteo SelectedMeteo

        {
            get
            {
                return _selectedMeteo;
            }
            set
            {
                _selectedMeteo = value;
                NotifyOfPropertyChange(() => SelectedCountry);
            }

        }
        public List<Language> Language
        {
            get
            {
                return _language;
            }

            set
            {
                _language = SelectedCountry.Languages;
                NotifyOfPropertyChange(() => Language);
            }
        }

        public ShellViewModel()
        {
            LoadData();
        }

        private async void LoadData()
        {
            await LoadApiCountries();


            //Is needed to select a country to get the Capital and it will add to Api name's city
            //if (SelectedCountry != null)
            //{
                //await LoadApiMeteo();
            //}
        }

        //Button Clear
        public void ClearText(string name, string capital)
        {
            //Name = string.Empty;
            //Capital = string.Empty;
        }

        //Disable Button when the text is clear
        public bool CanClearText(string name, string capital)
        {
            if (string.IsNullOrWhiteSpace(name) && string.IsNullOrWhiteSpace(capital))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private async void LoadCountries()
        {
            bool load;

            var connection = _networkService.CheckConnection();

            if (!connection.IsSuccess)
            {
                ///TODO LoadLocalCountries();
                //LoadLocalCountries();
                load = false;
            }
            else
            {
                await LoadApiCountries();
                load = true;
            }

            //if(Countries.Count == 0)
            //{
            //    MessageBoxResult result = MessageBox.Show("No Internet connection" + Environment.NewLine +
            //        "The Countries weren't loaded" + Environment.NewLine +
            //        "try it later!");

            //}
        }


        public async Task LoadApiCountries()
        {
            var response = await _apiService.GetCountries("http://restcountries.eu/rest/v2/all", "/rest/v2/all");

            Countries = (List<Country>)response.Result;

            ListToBindableCountry();

            //dataService.DeleteData();

            //dataService.SaveData(Countries);
        }



        //Method to renderize from List to Bindable
        public void ListToBindableCountry()
        {
            Country.AddRange(Countries);
        }

        public void LoadPageRegion()
        {
            if (SelectedCountry == null)
            {
                DialogService.ShowMessage("Atention!", "Select the Country you want, please");
                return;
            }
            
            ActivateItem(new GeneralPageViewModel(SelectedCountry));
            
        }

        public void LoadPageLanguages()
        {
            if (SelectedCountry == null)
            {
                DialogService.ShowMessage("Atention!","Select the Country you want, please");
                return;
            }
            ActivateItem(new LanguagesPageViewModel(SelectedCountry,Language));
        }

        public void LoadPageWeather()
        {
            if (SelectedCountry == null)
            {
                DialogService.ShowMessage("Atention!", "Select the Country you want, please");
                return;
            }
            ActivateItem(new WeatherViewModel(SelectedCountry,SelectedMeteo));
        }

    }
}
