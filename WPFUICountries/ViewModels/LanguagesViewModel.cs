﻿
namespace WPFUICountries.ViewModels
{
    using Caliburn.Micro;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using WPFUICountries.Models;

    public class LanguagesViewModel: Screen
    {
        private Country selectedCountry;

        public LanguagesViewModel(Country selectedCountry)
        {
            this.selectedCountry = selectedCountry;
        }
    }
}
